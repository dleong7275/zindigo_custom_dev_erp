*&---------------------------------------------------------------------*
*& Report  ZCDC_3PL_PO_OUTPUT
*&
*&---------------------------------------------------------------------*
*& PROGRAM ID           CDC_PO_01_I
*& AUTHOR               Desmond Leong
*& OWNER(Process Team)  Crystal Campbell
*& CREATE DATE          2017-12-27
*& R/3 RELEASE VERSION  ECC 6.0/EHP6
*& BASED-ON PROGRAM     N/A
*& DESCRIPTION          CDC - PO to 3PL (transloader) & (0988) XML output
*&---------------------------------------------------------------------*
*&**********************************************************************
*& VERSION CONTROL (Most recent on top):
*& DATE        AUTHOR           CTS REQ       DESCRIPTION
*& 2017-12-27  DLEONG           RD1K947871    PO XML Output to Transloader
*&**********************************************************************
report zcdc_3pl_po_output message-id zm_rep.

include rvadtabl.

*&---------------------------------------------------------------------*
*&      Form  entry_neu
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->ENT_RETCO  text
*      -->ENT_SCREEN text
*----------------------------------------------------------------------*
form entry_neu using ent_retco ent_screen.


  constants: c_po_xml_dtype type prx_r3name value 'ZSCIPOPURCHASE_ORDER',
             c_logical_file type filename-fileintern value 'Z_R3O_T',
             c_xml_ext      type c length 4 value '.xml'.


  data: l_druvo like t166k-druvo,
        l_nast  like nast,
        l_from_memory,
        l_doc   type meein_purchase_doc_print.

  data : lv_datatype   type prx_r3name.
  data : lv_xml        type xstring,
         ls_admin_info  type zscipoadmin_info_type,
         ls_headerinfo type zscipopoheader_type,
         ls_iteminfo   type zscipopoline_item_type,
         ls_ekko       type ekko,
         lt_ekpo       type standard table of ekpo,
         ls_ekpo       type ekpo,
         ls_podata     type zscipopurchase_order,
         lt_pay_load   type zscipopotype_tab,
         ls_pay_load   like line of lt_pay_load,
         ls_pekpo      type pekpo,
         lv_butxt      type butxt.

  data xml_writer type ref to cl_sxml_string_writer.

  data lr_conv type ref to cl_abap_conv_in_ce.
  data : lv_request_xml_xstr type xstring,
         ls_payload_out      type string,
         lv_server_tzone     type tznzonesys,
         lv_timestamp        type xsddatetime_long_z,
         ls_payload_out_xstring type xstring,
         lo_converter           type ref to cl_abap_conv_in_ce,
         lv_filename            type c length 132.

  field-symbols: <fs_ekpo> type ekpo.

  clear ent_retco.
  if nast-aende eq space.
    l_druvo = '1'.
  else.
    l_druvo = '2'.
  endif.

  call function 'ME_READ_PO_FOR_PRINTING'
    exporting
      ix_nast        = nast
      ix_screen      = ent_screen
    importing
      ex_retco       = ent_retco
      ex_nast        = l_nast
      doc            = l_doc
    changing
      cx_druvo       = l_druvo
      cx_from_memory = l_from_memory.


  check ent_retco eq 0.


* Map items, header, then admin
  loop at l_doc-xekpo assigning <fs_ekpo>.
    ls_iteminfo-line_num       = <fs_ekpo>-ebelp.

    call function 'CONVERSION_EXIT_MATN1_OUTPUT'
      exporting
        input  = <fs_ekpo>-matnr
      importing
        output = ls_iteminfo-material_num.

    ls_iteminfo-original_qty   = <fs_ekpo>-menge.
    ls_iteminfo-action_line    = '001'.
    ls_iteminfo-plant          = <fs_ekpo>-werks.
    append ls_iteminfo to ls_pay_load-podetail.
  endloop.


  ls_pay_load-poheader-ponum      = l_doc-xekko-ebeln.
  ls_pay_load-poheader-order_date = l_doc-xekko-bedat.
*  cl_abap_datfm=>conv_date_int_to_ext(
*    exporting
*      im_datint    =     " internal representation of date
*      im_datfmdes  =   '6'  " date format wanted for conversion
*    importing
*       ex_datext    =     " external representation of date
**      ex_datfmused =     " date format used for conversion
*).
**    catch cx_abap_datfm_format_unknown.    " Exception in Class CL_ABAP_DATFM - Format unknown

  ls_pay_load-poheader-potype = l_doc-xekko-bsart.

  sort l_doc-xpekpo by lfdat ascending.

  read table l_doc-xpekpo into ls_pekpo index 1.

  if sy-subrc = 0.
    ls_pay_load-poheader-delivery_date = ls_pekpo-lfdat.
  endif.

  append ls_pay_load to ls_podata-pay_load.

  ls_admin_info-doc_type    = l_doc-xekko-bsart.

  call function 'CONVERSION_EXIT_ALPHA_OUTPUT'
    exporting
      input  = l_doc-xekko-lifnr
    importing
      output = ls_admin_info-partner.


  call function 'NUMBER_GET_NEXT'
    exporting
      nr_range_nr             = '01'  " Number range number
      object                  = 'Z3PLPO'  " Name of number range object
*     quantity                = '1' " Number of numbers
*     subobject               = SPACE    " Value of sub-object
*     toyear                  = '0000' " Value of To-fiscal year
*     ignore_buffer           = SPACE    " Ignore object buffering
    importing
      number                  = ls_admin_info-trans_id   " free number
*     quantity                =     " Number of numbers
*     returncode              =     " Return code
    exceptions
      interval_not_found      = 1
      number_range_not_intern = 2
      object_not_found        = 3
      quantity_is_0           = 4
      quantity_is_not_1       = 5
      interval_overflow       = 6
      buffer_overflow         = 7
      others                  = 8.
  if sy-subrc <> 0.
*   message id sy-msgid type sy-msgty number sy-msgno
*              with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  endif.




  select single butxt into lv_butxt
    from t001
    where bukrs = l_doc-xekko-bukrs.

  if sy-subrc = 0.
    ls_admin_info-sender_id    = lv_butxt.
  endif.

  ls_admin_info-receiver_id  = l_nast-parnr.
  ls_admin_info-record_count = '1'.

  convert date l_doc-xekko-bedat time sy-uzeit
  into time stamp ls_admin_info-doc_date time zone 'EST_NA'.

  ls_podata-admin_info = ls_admin_info.
*------- Populate abap Data structure------------------------------

*------------------------------------------------------------------
  try.
*   This will generate the Simple Transformation from information
*   available in proxy tables and transform the abap input data
*   to XML with *namespace*.

* Which root data type ?
      lv_datatype = c_po_xml_dtype.    "Data type 'ZSCIPOPURCHASE_ORDER'
      lv_xml = cl_proxy_xml_transform=>abap_to_xml_xstring(
        abap_data  = ls_podata
        ddic_type  = lv_datatype
*      ext_xml    = ''
        xml_header = 'full'
      ).
*------------------------------------------------------------------
*   In case you need to strip the namespace information from tags
*   then create a simple transformation Z_RAM_REMOVE_NAMESPACEE_XSLT
*   in XSLT_TOOL use the statement below.

      xml_writer = cl_sxml_string_writer=>create( type = if_sxml=>co_xt_xml10 ).

      call transformation z_remove_ns
      source xml lv_xml
      result xml xml_writer.
*------------------------------------------------------------------
*     Write XML to file
      ls_payload_out_xstring = xml_writer->get_output( ).


      lo_converter = cl_abap_conv_in_ce=>create( input       = ls_payload_out_xstring
                                                 encoding    = 'UTF-8'
                                                 replacement = '?'
                                                 ignore_cerr = abap_true ).

      try.
          call method lo_converter->read( importing data = ls_payload_out ).

          concatenate 'PO' ls_admin_info-trans_id c_xml_ext into lv_filename.

          call function 'FILE_GET_NAME_USING_PATH'
            exporting
              client                     = sy-mandt
              logical_path               = c_logical_file
              operating_system           = sy-opsys
              file_name                  = lv_filename
            importing
              file_name_with_path        = lv_filename
            exceptions
              path_not_found             = 1
              missing_parameter          = 2
              operating_system_not_found = 3
              file_system_not_found      = 4
              others                     = 5.

          if sy-subrc <> 0.
            message e000 with 'Please provide a valid logical file path'.
          endif.

          open dataset lv_filename for output in text mode encoding default.
          if sy-subrc <> 0.
            message e000 with 'Please enter a proper file path and name'.
          endif.
          transfer ls_payload_out to lv_filename.
          close dataset lv_filename.

        catch cx_sy_conversion_codepage.
*-- Should ignore errors in code conversions
        catch cx_sy_codepage_converter_init.
*-- Should ignore errors in code conversions
        catch cx_parameter_invalid_type.
        catch cx_parameter_invalid_range.
      endtry.


*   Just to show the xml .
*      cl_srtg_helper=>write_utf8_xmldoc(
*        doc = lv_xml
*        use_html_control = 'X'
*      ).
*------------------------------------------------------------------
    catch cx_transformation_error .
    catch cx_root .                                      "#EC CATCH_ALL

  endtry.

endform.                    "entry_neu.
