class ZSCIPOCO_SCI_PURCHASE_ORDER definition
  public
  inheriting from CL_PROXY_CLIENT
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !LOGICAL_PORT_NAME type PRX_LOGICAL_PORT_NAME optional
    raising
      CX_AI_SYSTEM_FAULT .
  methods SCI_PURCHASE_ORDER
    importing
      !OUTPUT type ZSCIPOPURCHASE_ORDER
    raising
      CX_AI_SYSTEM_FAULT .
protected section.
private section.
ENDCLASS.



CLASS ZSCIPOCO_SCI_PURCHASE_ORDER IMPLEMENTATION.


method CONSTRUCTOR.

  super->constructor(
    class_name          = 'ZSCIPOCO_SCI_PURCHASE_ORDER'
    logical_port_name   = logical_port_name
  ).

endmethod.


method SCI_PURCHASE_ORDER.

  data:
    ls_parmbind type abap_parmbind,
    lt_parmbind type abap_parmbind_tab.

  ls_parmbind-name = 'OUTPUT'.
  ls_parmbind-kind = cl_abap_objectdescr=>importing.
  get reference of OUTPUT into ls_parmbind-value.
  insert ls_parmbind into table lt_parmbind.

  if_proxy_client~execute(
    exporting
      method_name = 'SCI_PURCHASE_ORDER'
    changing
      parmbind_tab = lt_parmbind
  ).

endmethod.
ENDCLASS.
