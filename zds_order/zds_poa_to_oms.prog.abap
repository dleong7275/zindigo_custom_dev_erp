*&---------------------------------------------------------------------*
*& Report  ZDS_POA_TO_OMS
*&
*&---------------------------------------------------------------------*
*& PROGRAM ID           ZDS_POA_TO_OMS
*& AUTHOR               Desmond Leong
*& OWNER(Process Team)  Jenny Ozcan
*& CREATE DATE          2018-1-12
*& R/3 RELEASE VERSION  ECC 6.0/EHP6
*& BASED-ON PROGRAM     N/A
*& DESCRIPTION          Drop Ship POA to OMS
*&---------------------------------------------------------------------*
*&**********************************************************************
*& VERSION CONTROL (Most recent on top):
*& DATE        AUTHOR           CTS REQ       DESCRIPTION
*& 2018-1-12  DLEONG           RD1K947898     Drop Ship POA to OMS
*&**********************************************************************
report zds_poa_to_oms message-id zm_rep.

include rvadtabl.

*&---------------------------------------------------------------------*
*&      Form  entry_neu
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->ENT_RETCO  text
*      -->ENT_SCREEN text
*----------------------------------------------------------------------*
form entry_neu using ent_retco ent_screen.

  types: begin of ty_oms_ln,
           posnr type posnr,
           posex type posex,
         end of ty_oms_ln.

  types: begin of ty_ekes,
           ebeln type ebeln,
           ebelp type ebelp,
           eindt type bbein,
           erdat type bberd,
           menge type bbmng,
           xblnr type xblnr_long,
         end of ty_ekes.

  data: l_druvo like t166k-druvo,
        l_nast  like nast,
        l_from_memory,
        l_doc   type meein_purchase_doc_print.

  data : lv_datatype   type prx_r3name.
  data : ls_poa        type zdsoms_poa,
         lt_poa_items  type zdsoms_poa_purchase_order__tab,
         ls_poa_items  like line of lt_poa_items,
         lcl_poa_proxy type ref to zdsco_poato_oms,
         lcl_system_fault type ref to cx_ai_system_fault,
         ls_output     type zdspoa_to_oms,
         xi_header_protocol  type ref to if_wsprotocol_xi_header,
         xi_msguid           type string,
         lv_msgv1    type symsgv,
         lt_posex    type standard table of ty_oms_ln,
         lt_ekes     type standard table of ty_ekes,
         lv_posex    type posex,
         lv_ebeln    type ebeln,
         lv_lifnr    type lifnr,
         lv_vbeln    type vbeln,
         lv_bstnk    type bstnk.



  field-symbols: <fs_ekpo> type ekpo,
                 <fs_ekkn> type ekkn,
                 <fs_eket> type eket,
                 <fs_posex> type ty_oms_ln,
                 <fs_ekes> type ty_ekes.

  clear ent_retco.
  if nast-aende eq space.
    l_druvo = '1'.
  else.
    l_druvo = '2'.
  endif.

  call function 'ME_READ_PO_FOR_PRINTING'
    exporting
      ix_nast        = nast
      ix_screen      = ent_screen
    importing
      ex_retco       = ent_retco
      ex_nast        = l_nast
      doc            = l_doc
    changing
      cx_druvo       = l_druvo
      cx_from_memory = l_from_memory.


  check ent_retco eq 0.

* Get OMS line number
  select posnr posex
    into table lt_posex
    from vbap
    for all entries in l_doc-xekkn
    where vbeln = l_doc-xekkn-vbeln
    and   posnr = l_doc-xekkn-vbelp.

  if sy-subrc <> 0.
    free: lt_posex.
  endif.

* Get Vendor POA
  select ebeln ebelp eindt erdat menge xblnr
    into table lt_ekes
    from ekes
    for all entries in l_doc-xekpo
    where ebeln = l_doc-xekpo-ebeln
    and   ebelp = l_doc-xekpo-ebelp.

  if sy-subrc <> 0.
    free: lt_ekes.
  endif.


* Map items, header, then admin
  loop at l_doc-xekpo assigning <fs_ekpo>.

    ls_poa_items-ebelp = <fs_ekpo>-ebelp.

    ls_poa_items-menge = <fs_ekpo>-menge.

    read table l_doc-xeket assigning <fs_eket>
      with key ebeln = <fs_ekpo>-ebeln
               ebelp = <fs_ekpo>-ebelp.

    if sy-subrc = 0.
      ls_poa_items-eindt = <fs_eket>-eindt.
    endif.

    read table l_doc-xekkn assigning <fs_ekkn>
      with key ebeln = <fs_ekpo>-ebeln
               ebelp = <fs_ekpo>-ebelp.
    if sy-subrc = 0.
      ls_poa_items-veten = <fs_ekkn>-veten.

      read table lt_posex assigning <fs_posex> with key
                                    posnr = <fs_ekkn>-vbelp.
      if sy-subrc = 0.
        call function 'CONVERSION_EXIT_ALPHA_OUTPUT'
          exporting
            input  = <fs_posex>-posex
          importing
            output = lv_posex.
        ls_poa_items-posex = lv_posex.
      endif.
    endif.

    read table lt_ekes assigning <fs_ekes> with key
                                  ebelp = <fs_ekpo>-ebelp.
    if sy-subrc = 0.
      ls_poa_items-ackcode       = <fs_ekes>-xblnr.
      ls_poa_items-ekes_menge    = <fs_ekes>-menge.
      ls_poa_items-ekes_eindt    = <fs_ekes>-eindt.
      ls_poa_items-ekes_erdat    = <fs_ekes>-erdat.
    endif.

    call function 'CONVERSION_EXIT_MATN1_OUTPUT'
      exporting
        input  = <fs_ekpo>-matnr
      importing
        output = ls_poa_items-matnr.

    append ls_poa_items to lt_poa_items.
  endloop.


  ls_poa-purchase_order_items[] = lt_poa_items.

  read table l_doc-xekkn assigning <fs_ekkn> index 1.
  if sy-subrc = 0.
    call function 'CONVERSION_EXIT_ALPHA_OUTPUT'
      exporting
        input  = <fs_ekkn>-vbeln
      importing
        output = lv_vbeln.
    ls_poa-vbeln = lv_vbeln.

    select single bstnk into lv_bstnk
      from vbak
      where vbeln = <fs_ekkn>-vbeln.
    if sy-subrc = 0.
      ls_poa-order_id = lv_bstnk.
    endif.
  endif.


  call function 'CONVERSION_EXIT_ALPHA_OUTPUT'
    exporting
      input  = l_doc-xekko-ebeln
    importing
      output = lv_ebeln.
  ls_poa-ebeln = lv_ebeln.

  call function 'CONVERSION_EXIT_ALPHA_OUTPUT'
    exporting
      input  = l_doc-xekko-lifnr
    importing
      output = lv_lifnr.
  ls_poa-lifnr = lv_lifnr.
  ls_output-poa_to_oms = ls_poa.



  try.
      create object lcl_poa_proxy.
      call method lcl_poa_proxy->poato_oms
        exporting
          output = ls_output.

    catch cx_ai_system_fault into lcl_system_fault.
      message lcl_system_fault->errortext type 'E'.
  endtry.


  try.
      xi_header_protocol ?=  lcl_poa_proxy->get_protocol( if_wsprotocol=>xi_header ).

      xi_msguid = xi_header_protocol->get_header_field( if_wsprotocol_xi_header=>message_id ).

    catch cx_ai_system_fault into lcl_system_fault.
      message lcl_system_fault->errortext type 'E'.
  endtry.




  lv_msgv1 = xi_msguid.

  call function 'NAST_PROTOCOL_UPDATE'
    exporting
      msg_arbgb              = 'ZSD'
      msg_nr                 = '029'
      msg_ty                 = 'I'
      msg_v1                 = lv_msgv1
*     MSG_V2                 = ' '
*     MSG_V3                 = ' '
*     MSG_V4                 = ' '
    exceptions
      message_type_not_valid = 1
      no_sy_message          = 2
      others                 = 3.
  if sy-subrc <> 0.
* Implement suitable error handling here
  endif.

endform.                    "entry_neu.
