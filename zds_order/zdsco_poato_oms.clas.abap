class ZDSCO_POATO_OMS definition
  public
  inheriting from CL_PROXY_CLIENT
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !LOGICAL_PORT_NAME type PRX_LOGICAL_PORT_NAME optional
    raising
      CX_AI_SYSTEM_FAULT .
  methods POATO_OMS
    importing
      !OUTPUT type ZDSPOA_TO_OMS
    raising
      CX_AI_SYSTEM_FAULT .
protected section.
private section.
ENDCLASS.



CLASS ZDSCO_POATO_OMS IMPLEMENTATION.


method CONSTRUCTOR.

  super->constructor(
    class_name          = 'ZDSCO_POATO_OMS'
    logical_port_name   = logical_port_name
  ).

endmethod.


method POATO_OMS.

  data:
    ls_parmbind type abap_parmbind,
    lt_parmbind type abap_parmbind_tab.

  ls_parmbind-name = 'OUTPUT'.
  ls_parmbind-kind = cl_abap_objectdescr=>importing.
  get reference of OUTPUT into ls_parmbind-value.
  insert ls_parmbind into table lt_parmbind.

  if_proxy_client~execute(
    exporting
      method_name = 'POATO_OMS'
    changing
      parmbind_tab = lt_parmbind
  ).

endmethod.
ENDCLASS.
